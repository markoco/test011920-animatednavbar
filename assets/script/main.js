window.addEventListener('scroll', function(){
    const logoImage = document.querySelector(".logo img");
    const mainNav = document.getElementById("mainNav");

    if(window.pageYOffset > 0){
        logoImage.style.height = "64px"; //approach 1 to change element style
        mainNav.classList.add('bg-black'); //approach 2 to change element style
        mainNav.classList.add('txt-white');
    }
    else{
        logoImage.style.height = "84px";
        mainNav.classList.remove('bg-black');
        mainNav.classList.remove('txt-white');
    }
})